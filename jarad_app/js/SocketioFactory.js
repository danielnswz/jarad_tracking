app.service('socketio', ['$rootScope', '$window', 'API_URL', function($rootScope, $window, API_URL) {
    'use strict';
    var token = 1;
    var user_data = JSON.parse($window.localStorage.getItem('app_user'));
    
    if (user_data)
        token = user_data.token;
    
    var opts = {
        'forceNew': true,
        query: "web=true&token=" + token
    };
/*
    'reconnection': true,
    'reconnectionDelay': 1000,
    'reconnectionDelayMax': 5000,
    'reconnectionAttempts': 5
*/

    var socket = io.connect(API_URL, opts);

    //"", { query: "token=cKM-CJY5M24:APA91bEXTQC4yIF5LG-bxvS7P1fnbAnjGQpVXa8oZkcCDEB4Eu_yaPqv3Mdpg4W-icgTlt0D-2AWpcRAzXdCj9KvFkpdk6PPR2dR0SG9T4hdyu7ayo3l8A7w-4nPyqAKreZrWUXigLLj" });

    this.on = function(eventName, callback) {
        socket.on(eventName, function() {
            var args = arguments;
            $rootScope.$applyAsync(function() {
                callback.apply(socket, args);
            });
        });
    }

    this.emit = function(eventName, data, callback) {
        socket.emit(eventName, data, function() {
            var args = arguments;
            $rootScope.$applyAsync(function() {
                if (callback) {
                    callback.apply(socket, args);
                }
            });
        });
    }

    this.disconnect = function() {
        // this.emit("disconnection");
        socket.disconnect();
    }
}]);
