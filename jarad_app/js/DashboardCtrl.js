app.controller('DashController', DashController);

DashController.$inject = ['$http', 'API_URL', '$rootScope', '$q', '$scope', '$uibModal', 'socketio', '$window', '$location'];

function DashController($http, API_URL, $rootScope, $q, $scope, $uibModal, socketio, $window, $location) {
    var vm = this;

    postconstructor();
    socketService();

    function postconstructor() {
        vm.showModalRegDevices = false;
        vm.selected_device = false;
        vm.option = 0;
        vm.options = [{
            name: "Home",
            class: "glyphicon glyphicon-star",
            arrow: true
        }, {
            name: "Llamadas",
            class: "glyphicon glyphicon-earphone",
            arrow: false
        }, {
            name: "Mensajes",
            class: "glyphicon glyphicon-envelope",
            arrow: false
        }, {
            name: "Fotos",
            class: "glyphicon glyphicon-camera",
            arrow: false
        }, {
            name: "Notificaciones",
            class: "glyphicon glyphicon-volume-up",
            arrow: false
        }, {
            name: "Contactos",
            class: "glyphicon glyphicon-user",
            arrow: false
        }];
        vm.activities = [];
        vm.mensajes = [];
        vm.llamadas = [];
        vm.contactos = [];
        vm.devices = [];
        vm.pictures = [];

        vm.modalReg = modalReg;
        vm.modalConf = modalReg;
        vm.modalUsers = modalUsers;
        vm.showPic = showPic;
        vm.Logout = Logout;
        vm.regUser = regUser;

        vm.setOption = setOption;
        vm.openMessage = openMessage;
        vm.onClickDevice = onClickDevice;
        vm.req_device = req_device;
        vm.delDevice = delDevice;
        vm.addDevice = addDevice;
        vm.panico = panico;
    }

    function regUser() {
        var data = {
            username: vm.username,
            password: vm.password,
            full_name: vm.full_name
        };
        $http.post(API_URL + '/signin', data, function(rs) {
            console.log(rs);
        });
    }

    function modalUsers() {
        vm.modalUsers = $uibModal.open({
            animation: true,
            templateUrl: 'views/utils/modals/regUsers.html',
            scope: $scope
        });
    }

    function showPic(picture) {
        vm.picture = picture;
        vm.picture.src = API_URL + '/photo/' + vm.selected_device.telefono + '/' + picture.event;
        vm.modalPicture = $uibModal.open({
            animation: true,
            templateUrl: 'views/utils/modals/showPic.html',
            scope: $scope,
            size: 'lg'
        });
    }

    function openMessage(conversacion) {
        vm.conversacion = conversacion;
        vm.modalConversation = $uibModal.open({
            animation: true,
            templateUrl: 'views/utils/modals/showConversation.html',
            scope: $scope,
            size: 'lg'
        });
    }

    function setOption($index, option) {
        vm.options[vm.option].arrow = false;
        vm.option = $index;
        vm.options[$index].arrow = true;
    }

    function Logout() {
        socketio.disconnect();
        $window.localStorage.clear();
        $location.path('/');
    }

    function modalReg() {
        vm.modalreg = $uibModal.open({
            animation: true,
            templateUrl: 'views/utils/modals/regDevice.html',
            scope: $scope
        });
    }

    function onClickDevice(event, idDevice, device) {
        vm.selected_device = vm.selected_device == device ? false : device;
        if (vm.selected_device)
            req_device(vm.selected_device.token);
        else
            vm.option = 0;
    }

    function socketService() {
        socketio.on('error', function(msg) {
            console.log("unauthorized: " + JSON.stringify(msg));
            Logout();
        })
        socketio.on("socketid", function(data) {
            console.log('Conexión establecida. SocketID: ', data);
        });

        socketio.on("broadcast:locations", function(data) {
            console.log("broadcast:locations");
            // console.log(data);
            vm.devices = data;
            console.log(vm.devices);
            AddIconByStatus(vm.devices);
        });
        socketio.on("broadcast:location", function(data) {
            var flag_found = false;
            if (vm.devices.length != 0) {
                vm.devices.forEach(function(element, index) {
                    if (element.token == data.token) {
                        flag_found = true;
                        vm.devices[index].position = data.position
                            // index--;
                    }
                });
            }
            if (vm.selected_device.token == data.token)
                vm.selected_device.position = data.position;
            // vm.devices.push(data);
            AddIconByStatus(vm.devices);
            // console.log(data);
        });

        socketio.on("broadcast:message", function(data) {
            if (vm.selected_device === data.device.token) {
                vm.mensajes.push(data);
                // AddPreAndLogo(vm.mensajes);
            }
        });

        socketio.on("broadcast:event", function(data) {
            if (vm.selected_device === data.device.token) {
                vm.activities.push(data);
                vm.activities.forEach(function(element, index) {
                    if (element.type == 'ENTRADA' || element.type == 'SALIDA' || element.type == 'PERDIDA') {
                        vm.llamadas.push(element);
                    }
                    if (element.type == 'PICTURE') {
                        vm.pictures.push(element);
                    }
                });
            }
        });

        socketio.on("mobile:left", function(data) {
            vm.devices.forEach(function(element) {
                if (element.token == data)
                    element.online = false;
            });
            AddIconByStatus(vm.devices);
        });
    }

    function AddIconByStatus(json) {
        // console.log(typeof(json));

        json.forEach(function(device) {
            if (device.online) {
                if (device.status) {
                    device.icon = "./src/red-spot.png";
                    device.animation = "BOUNCE";
                } else {
                    device.icon = "./src/blue-spot.png"
                    device.animation = "none";
                }
            } else {
                device.icon = "./src/gray-spot.png"
                device.animation = "none";
            }
        })
    }

    function panico() {
        if (vm.selected_device.status) {
            var text = "Desactivó el pánico del dispositivo";

        } else {
            var text = "Activó el pánico del dispositivo";
        }

        vm.selected_device.status = !vm.selected_device.status;
        socketio.emit("panic:web", {
            token: vm.selected_device.token,
            status: !vm.selected_device.status
        });
        AddIconByStatus(vm.devices);
    }

    function addDevice(num) {
        // console.log('añadir telefono');
        socketio.emit('reg:device', {
            telefono: num
        }, function(data) {
            if (data.success) {
                vm.devices.push(data.device);
                console.log(data.device);
                AddIconByStatus(vm.devices);
            } else {
                console.log(data.msg);
            }
        });
    }

    function delDevice(device) {
        vm.devices.splice(device, 1);
        socketio.emit('del:device', {
            telefono: device.telefono
        });
    }

    function req_events_pic_calls(token) {
        console.log('req_events_pic_calls');
        socketio.emit('req:device_events', {
            token: token
        }, function(data1) {
            if (data1 !== 'undefined') {
                vm.activities = data1;
                console.log(vm.activities);
                vm.activities.forEach(function(element, index) {
                    if (element.type == 'ENTRADA' || element.type == 'SALIDA' || element.type == 'PERDIDA') {
                        vm.llamadas.push(element);
                    }
                    if (element.type == 'PICTURE') {
                        vm.pictures.push(element);
                    }
                });

            }
        });
    }

    function req_messages(token) {
        socketio.emit('req:device_messages', {
            token: token
        }, function(data2) {
            vm.mensajes = data2;
        });
    }

    /*function req_calls(token) {
        if (vm.activities.length > 0) {
            vm.activities.forEach(function(element, index) {
                if (element.type == 'entrante' || element.type == 'saliente' || element.type == 'perdida') {
                    vm.llamadas.push(element);
                }
            });
        }
    }*/

    function req_contacts(token) {
        socketio.emit('req:contactos', {
            token: token
        }, function(data3) {
            vm.contactos = data3;
        });
    }

    function req_device(token) {
        switch (vm.option) {
            case 0:
                // code block
                break;
            case 1:
                // code block
                if (vm.llamadas.length > 0)
                    vm.llamadas = [];
                    req_events_pic_calls(token);
                break;
            case 2:
                // code block
                if (vm.mensajes.length > 0)
                    vm.mensajes = [];
                req_messages(token);
                break;
            case 3:
                if (vm.pictures.length > 0)
                    vm.pictures = [];
                req_events_pic_calls(token);
                break;
            case 4:
                if (vm.activities.length > 0)
                    vm.activities = [];
                req_events_pic_calls(token);
                break;
            case 5:
                if (vm.contactos.length > 0)
                    vm.contactos = [];
                req_contacts(token);
                break;
        }
    }
}
