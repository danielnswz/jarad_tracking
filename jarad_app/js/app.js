var app =
    angular.module('realtimeData', ['ngRoute',
        'ngMap', 'angular.filter', 'ui.bootstrap'
    ]);

app.constant('API_URL', 'http://localhost:3000/web')//'http://017dea03.ngrok.io/web') 
    .config(['$routeProvider', function($routeProvider) {
        'use strict';

        $routeProvider
            .when('/', {
                // controller: 'LoginController',
                templateUrl: 'views/login.html'
            })
            .when('/dashboard', {
                cache: false,
                templateUrl: 'views/dashboard2.html',
                resolve: ['socketio']
            })
            .otherwise({
                redirectTo: '/'
            });
    }])
    .filter('reverse', function() {
        'use strict';

        return function(items) {
            return items.slice().reverse();
        };
    });
