 app.controller('LoginController', ['$scope', '$location', '$http', 'API_URL', '$window', function($scope, $location, $http, API_URL, $window) {
     'use strict';
     console.log('LoginController');
     
     $scope.login = function() {
         console.log('Logging');
         var data = {
             username: $scope.username,
             password: $scope.password
         };
         $http.post(API_URL + '/authenticate', data).then(function(data) {
             if (data.data.type) {
                 // LOGGED IN
                 $window.localStorage.setItem('app_user', JSON.stringify(data.data));
                 $location.path('/dashboard');
             } else {
                 // CREDENCIALES INVALIDAS
             }
         });
     }
 }]);
