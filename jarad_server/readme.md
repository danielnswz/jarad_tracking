## JARAD API

JARAD API es una interfaz enfocada en proveer un sistema de comunicación en tiempo real para una aplicación web y movil que comunicará datos como: contactos, mensajes, ubicación, entre otros.

## Official Documentation

La documentación para [socketio](http://socket.io/docs/)
La documentación para [express](http://expressjs.com/es/)
La documentación para [nodejs](https://nodejs.org/en/)


### License

JARAD API es un software de codigo abierto bajo la licencia [MIT license](http://opensource.org/licenses/MIT)
