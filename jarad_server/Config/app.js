var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var secret = require('./secret');
var fileUpload = require('express-fileupload');
var socketioJwt = require("socketio-jwt");
var webCon_sockets = require("../Controllers/webCon_sockets");
var mobCon_sockets = require("../Controllers/mobCon_sockets");


/*Controllers*/
var Auth = require('../Controllers/AuthCtrl.js');


process.env.JWT_SECRET = secret.secretToken;

app.use(function(req, res, next) {
    res.io = io;
    next();
});

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser({uploadDir:'/archivos'}));
app.use(fileUpload());
var resolveCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header("Access-Control-Allow-Headers", "Content-Type, Authorization");
    res.header("Access-Control-Allow-Credentials", true);

    if ('OPTIONS' == req.method) {
        res.sendStatus(200);
    } else {
        next();
    }
};
app.use(resolveCrossDomain);


var mob = io.of('/mobile');

mob.on('connection', function(socket) {
    console.log('Mobile connection!');
    web.emit('socketid', 'Es una conexion movil! SocketID: ' + socket.id);
    mobCon_sockets.sockets(socket, web);
});

app.use('/web/authenticate', Auth.authenticate);
app.use('/web/signin', Auth.signin);



var web = io.of('/web')
    .use(socketioJwt.authorize({
        secret: process.env.JWT_SECRET,
        handshake: true
    }));
web.on('connection', function(socket) {
    console.log('Web connection!');
    web.emit('socketid', socket.id);
    webCon_sockets.sockets(socket, mob);
    socket.on('disconnect', function() {
        console.log('Web disconnection!');
    });

});
/*PICTURE CONTROLLER*/
var Picture = require('../Controllers/PicCtrl.js')(web);
app.post('/mobile/photo', Picture.create);
app.get('/web/photo/:telefono/:imagen', Picture.show);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.send({
            message: err.message,
            error: err
        });
        return;
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.send({
        message: err.message,
        error: {}
    });
    return;
});


module.exports = { app: app, server: server };
