var mongoose = require("mongoose");
var mongo_url = "mongodb://127.0.0.1/jarad";


mongoose.connect(mongo_url, function(err) {
    if (err)
        console.log(err);
    else
        console.log('Conectado a mongodb!');
});


var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

var adminSchema = new Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    full_name: String,
    devices: [{
        type: ObjectId,
        ref: 'Device'
    }]
});

var deviceSchema = new Schema({
    nombre: {
        type: String,
        required: true
    },
    telefono: {
        type: String,
        required: true
    },
    token: {
        type: String,
        required: true
    },
    latitud: {
        type: Number,
        default: 0
    },
    longitud: {
        type: Number,
        default: 0
    },
    status: {
        type: Boolean,
        default: false
    },
    mails: {
        type: Array
    },
    emergencyMsg: {
        type: String,
        default: "Me paso algo, ayuda!"
    },
    socketid: {
        type: String,
        required: true
    },
    online: {
        type: Boolean,
        default: true
    }
});

var eventSchema = new Schema({
    event: {
        type: String,
        required: true
    },
    time: {
        type: Date,
        default: Date.now
    },
    type: {
        type: String,
        required: true
    },
    device: {
        type: ObjectId,
        ref: 'Device'
    }
});

var smsSchema = new Schema({
    sms: {
        type: String,
        required: true
    },
    num: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    time: {
        type: Date,
        default: Date.now
    },
    device: {
        type: ObjectId,
        ref: 'Device'
    }
});

var contacSchema = new Schema({
    telefono: {
        type: String,
        required: true
    },
    nombre: {
        type: String,
        required: true
    },
    device: {
        type: ObjectId,
        ref: 'Device'
    }
});

//Define Models
var Admin = mongoose.model("User", adminSchema);
var Contact = mongoose.model("Contact", contacSchema);
var Message = mongoose.model("Message", smsSchema);
var Event = mongoose.model("Event", eventSchema);
var Device = mongoose.model("Device", deviceSchema);
// var postModel = mongoose.model('Post', Post);


// Export Models
exports.Admin = Admin;
exports.Contact = Contact;
exports.Message = Message;
exports.Event = Event;
exports.Device = Device;
// exports.postModel = postModel;
