var db = require('../Config/mongo_database');
var mailer = require('../Config/mailer');
Admin = db.Admin;
Device = db.Device;
Contact = db.Contact;
Message = db.Message;
Event = db.Event;

var broadcastDevices = function(socket) {
    Admin.findOne({
        username: socket.decoded_token._doc.username
    }).populate({
        path: 'devices'
    }).exec(function(err, docs) {
        if (err)
            console.log(err);
        else {
            /*var docs = docs.devices.filter(function(element) {
                return element.online != false;
            });*/
            // ELIMINAR ESTA LINEA SI SE ACTIVA EL FILTRO DE ARRIBA
            docs = docs.devices;
            if (docs === null)
                docs = [];
            socket.emit('broadcast:locations', docs);
            console.log('broadcast:locations');
            // console.log(docs);
        }
    });
}

var nodemailer = require('nodemailer');

var handleMails = function (req) {
    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        host: 'smtp.gmail.com',
        secure: false,
        port: 25,
        auth: {
            user: 'danielnswz@gmail.com', // Your email id
            pass: 'kmvfrziq' // Your password
        },
        tls: {
            rejectUnauthorized: false
        }
    });
    var text = req.emergencyMsg;
    //validar que lista de strings no este vacía!
    var mailOptions = {
        from: '<no-reply@jaradtracking.com>', // sender address
        to: req.mails.toString(), // list of receivers
        subject: 'PELIGRO <> JaradTracking <> ' + req.nombre, // Subject line
        text: text //, // plaintext body
            // html: '<b>Hello world ✔</b>' // You can choose to send an HTML body instead
    };
    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log("/sendmail error");
            console.log(error);
            return;
        } else {
            console.log('Message sent: ' + info.response);
        };
    });
    // setTimeout(handleMails, 10 * 1000);
};

exports.sockets = function(socket, mob) {
    broadcastDevices(socket);

    socket.on('reg:device', function(data, callback) {
        console.log('reg:device', data);
        Device.findOne({
            telefono: data.telefono
        }, function(err, doc) {
            if (err)
                console.log(err);
            else
            if (doc) {
                Admin.findOne({
                    username: socket.decoded_token._doc.username
                }).populate({
                    path: 'devices',
                    match: {
                        telefono: {
                            $eq: data.telefono
                        }
                    }
                }).exec(function(err, check) {
                    if (err)
                        console.log(err);
                    else {
                        var check = check.devices.filter(function(element) {
                            return element.telefono !== null;
                        });
                        console.log('check');
                        console.log(check.length);
                        if (check.length > 0) {
                            callback({
                                success: false,
                                msg: 'El dispositivo ya está asociado!'
                            });
                        } else {
                            Admin.findOne({
                                username: socket.decoded_token._doc.username
                            }).update({}, {
                                $push: {
                                    devices: doc._id
                                }
                            }, {
                                upsert: true
                            }, function(err) {
                                if (err) {
                                    console.log(err);
                                    callback({
                                        success: false,
                                        msg: 'Hubo un error registrando el dispositivo, Por favor contacte al administrador!'
                                    });
                                } else {
                                    callback({
                                        success: true,
                                        device: doc
                                    });
                                }
                            });
                        }
                    }
                });

            } else
                callback({
                    success: false,
                    msg: 'El número no está registrado!'
                });
        });
    });

    socket.on('del:device', function(data, callback) {
        console.log('del:device', data);
        Device.findOne({
            telefono: data.telefono
        }, function(err, doc) {
            if (err)
                console.log(err);
            else
            if (doc) {
                Admin.findOne({
                    username: socket.decoded_token._doc.username
                }).populate({
                    path: 'devices',
                    match: {
                        telefono: {
                            $eq: data.telefono
                        }
                    }
                }).exec(function(err, check) {
                    if (err)
                        console.log(err);
                    else {
                        var check = check.devices.filter(function(element) {
                            return element.telefono !== null;
                        });
                        console.log('check.length');
                        console.log(check);
                        if (check.length > 0) {
                            Admin.findOne({
                                username: socket.decoded_token._doc.username
                            }).update({}, {
                                $pull: {
                                    devices: doc._id
                                }
                            }, function(err) {
                                if (err) {
                                    console.log(err);
                                    callback({
                                        success: false,
                                        msg: 'Hubo un error eliminando el dispositivo, Por favor contacte al administrador!'
                                    });
                                } else {
                                    callback({
                                        success: true
                                    });
                                }
                            });
                        } else {
                            callback({
                                success: false,
                                msg: 'El dispositivo no está asociado!'
                            })
                        }
                    }
                });
            } else
                callback({
                    success: false,
                    msg: 'El número no está registrado!'
                });
        });
    });

    socket.on('req:contactos', function(data, callback) {
        console.log('req:contactos', data);
        Contact.find().lean().populate({
            path: 'device',
            match: {
                token: {
                    $eq: data.token
                }
            }
        }).exec(function(err, docs) {
            if (err)
                console.log(err);
            else {
                var docs = docs.filter(function(element) {
                    if (element.device !== null) {
                        delete element.device;
                        return element;
                    }
                });
                callback(docs);
            }
        });
    });

    socket.on('req:device_events', function(data, callback) {
        console.log('req:device_events', data);
        Event.find().lean().populate({
            path: 'device',
            match: {
                token: {
                    $eq: data.token
                }
            }
        }).exec(function(err, docs) {
            if (err)
                console.log(err);
            else {
                var docs = docs.filter(function(element) {
                    if (element.device !== null) {
                        delete element.device;
                        return element;
                    }
                });
                callback(docs);
            }
        });
    });

    socket.on('req:device_messages', function(data, callback) {
        console.log('req:device_messages', data);
        Message.find().lean().populate({
            path: 'device',
            match: {
                token: {
                    $eq: data.token
                }
            }
        }).exec(function(err, docs) {
            if (err)
                console.log(err);
            else {
                var docs = docs.filter(function(element) {
                    if (element.device !== null) {
                        delete element.device;
                        return element;
                    }
                });
                callback(docs);
            }
        });
    });

    var interval;
    socket.on('panic:web', function(data) {
        console.log('panic:web', data);
        var query = {
            token: data.token
        };
        Device.findOne(query, function(err, doc) {
            if (err)
                console.log(err);
            else {
                if (doc) {
                    doc.status = !doc.status;
                    doc.save(function(err) {
                        if (err)
                            console.log(err);
                        else {
                            console.log("Emitiendo panico exitosamente! >> " + doc.status);
                            mob.to(doc.socketid).emit('event:panic', {
                                status: doc.status
                            });
                            if (doc.status) {
                                interval = setInterval(function() {
                                    handleMails({
                                        emergencyMsg: doc.emergencyMsg + " [" + doc.latitud + "," + doc.longitud + "]",
                                        mails: doc.mails,
                                        nombre: doc.nombre
                                    });
                                }, 10 * 1000);
                            } else {
                                clearInterval(interval);
                            }
                        }
                    });
                } else {
                    console.log('No se consiguio el token!');
                }
            }
        });
    });
}
