var db = require('../Config/mongo_database');
var jwt = require('jsonwebtoken');

Admin = db.Admin;


exports.authenticate = function(req, res) {
    Admin.findOne({ username: req.body.username, password: req.body.password }, function(err, user) {
        if (err) {
            res.json({
                type: false,
                data: "Error occured: " + err
            });
        } else {
            if (user) {
                // if user is found and password is right
                // create a token
                var token = jwt.sign(user, process.env.JWT_SECRET, {
                    expiresIn: '1h' // expires in 30Minutes -- 1h si es en horas
                });
                res.json({
                    type: true,
                    data: {username: user.username, full_name: user.full_name},
                    token: token
                });
            } else {
                res.json({
                    type: false,
                    data: "Incorrect username/password"
                });
            }
        }
    });
};

exports.signin = function(req, res) {
    Admin.findOne({ username: req.body.username, password: req.body.password }, function(err, user) {
        if (err) { /*Error en consulta*/
            res.json({
                type: false,
                data: "Error occured: " + err
            });
        } else {
            if (user) { /*Usuario existe*/
                res.json({
                    type: false,
                    data: "User already exists!"
                });
            } else { /*Usuario no existe*/
                // console.log('req', req.body);
                user = new Admin({
                    username: req.body.username,
                    password: req.body.password,
                    full_name: req.body.full_name
                });
                user.save();
                res.json({
                    type: true,
                    data: "Usuario registrado exitosamente!"
                });
            }
        }
    });
};

exports.middleware = function(req, res, next) {
    // check header or url parameters or post parameters for token
    var token = req.body.token || req.query.token || req.headers['authorization'];
    // decode token
    if (token) {
        var bearer = token.split(' ');
        token = bearer[1];
        // verifies secret and checks exp
        jwt.verify(token, process.env.JWT_SECRET, function(err, decoded) {
            if (err) {
                return res.json({ success: false, message: 'Failed to authenticate token.' });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });

    } else {
        // if there is no token
        // return an error
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });

    }
};
