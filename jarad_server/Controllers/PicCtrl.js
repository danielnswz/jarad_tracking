var db = require('../Config/mongo_database');

Device = db.Device;
Event = db.Event;

module.exports = function(web) {
    var Picture = {};

    Picture.create = function(req, res) {
        /*CODIGO DE CREAR LA IMAGEN*/
        var imagen;
        var mimetypes = ['image/jpeg','image/png'];

        if (!req.files.imagen) {
            res.send('No files were uploaded.');
            return;
        }

        if(mimetypes.indexOf(req.files.imagen.mimetype)==-1){
            res.send('No valid file type');
            return;
        }

        if (!req.body.token) {
            res.send('No token provided.');
            return;
        }

        Device.findOne({
                token: req.body.token
            },
            function(err, doc) {
                if (err) {
                    res.send('Invalid token provided.');
                    return;
                }

                var d = new Date();
                var dir = './archivos/' + doc.telefono;
                // DIRECTORIO DE ESTE TELEFONO, SI NO EXISTE LO CREA
                if (!fs.existsSync(dir)) {
                    fs.mkdirSync(dir);
                }
                var fileName = d.getTime() + '.jpg';
                var filePath = dir + '/' + fileName;
                imagen = req.files.imagen;
                imagen.mv(filePath, function(err) {
                    if (err) {
                        res.status(500).send(err);
                    } else {
                        res.send('File uploaded!');
                        var e = new Event({
                            event: fileName,
                            type: 'PICTURE',
                            device: doc._id
                        });
                        web.emit('broadcast:event', {
                            event: fileName,
                            type: 'PICTURE',
                            time: e.time,
                            device: {
                                token: doc.token
                            }
                        });
                    }
                });

            });
    };

    Picture.show = function(req, res) {
        console.log(req.params);
        var telefono = req.params.telefono;
        var imagen = req.params.imagen;

        var dir = './archivos/' + telefono;

        res.sendfile(dir+'/'+imagen);
    };
    
    return Picture;
};
