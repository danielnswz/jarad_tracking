var db = require('../Config/mongo_database');
var mailer = require('../Config/mailer');
var fs = require("fs");
Admin = db.Admin;
Device = db.Device;
Contact = db.Contact;
Message = db.Message;
Event = db.Event;

// function to create file from base64 encoded string
function base64_decode(base64str, file) {
    // create buffer object from base64 encoded string, it is important to tell the constructor that the string is base64 encoded
    var bitmap = new Buffer(base64str, 'base64');
    // write buffer to file

    fs.writeFileSync(file, bitmap);
    console.log('******** File created from base64 encoded string ********');

    /*fs.writeFileSync(file, bitmap), function (err) {
        if (err) console.log(err);
        console.log('******** File created from base64 encoded string ********');
    });*/
}

var handleMails = function(req) {
    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        host: 'smtp.gmail.com',
        secure: false,
        port: 25,
        auth: {
            user: 'danielnswz@gmail.com', // Your email id
            pass: 'kmvfrziq' // Your password
        },
        tls: {
            rejectUnauthorized: false
        }
    });
    var text = req.emergencyMsg;
    //validar que lista de strings no este vacía!
    var mailOptions = {
        from: '<no-reply@jaradtracking.com>', // sender address
        to: req.mails.toString(), // list of receivers
        subject: 'PELIGRO <> JaradTracking <> ' + req.nombre, // Subject line
        text: text //, // plaintext body
            // html: '<b>Hello world ✔</b>' // You can choose to send an HTML body instead
    };
    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log("/sendmail error");
            console.log(error);
            return;
        } else {
            console.log('Message sent: ' + info.response);
        };
    });
    // setTimeout(handleMails, 10 * 1000);
};

exports.sockets = function(socket, web) {
    var token = socket.handshake.query.token;
    var query = {
        token: token
    };
    Device.findOne(query, function(err, doc) {
        if (err)
            console.log(err);
        else {
            if (!doc) { //puede crear un nuevo device
                socket.on('new:device', function(data, callback) {
                    console.log('new:device', data);
                    doc = new Device({
                        nombre: data.nombre,
                        telefono: data.telefono,
                        token: token,
                        mails: data.mails,
                        socketid: socket.id
                    });
                    doc.save(function(err) {
                        if (err) {
                            //console.log(err);
                            callback({
                                success: false
                            });
                        } else {
                            console.log('Usuario ' + data.nombre + ' registrado!!!');
                            callback({
                                success: true
                            });
                        }
                    });
                });
            } else { //actualizar socket:session_id en bdd
                doc.socketid = socket.id;
                doc.online = true;
                doc.save(function(err) {
                    if (err)
                        console.log(err);
                    else
                        console.log('Disp: ' + doc.nombre + ' socketid: ' + doc.socketid);
                });
            }

            //AQUI VAN EL RESTO DE LOS CANALES PARA ESTE MOVIL!!
            socket.on('map:location', function(data) {
                console.log('map:location', data);
                doc.latitud = data.latitud;
                doc.longitud = data.longitud;
                doc.save(function(err) {
                    if (err)
                        console.log(err);
                    else {
                        console.log(doc.nombre + ' en: [' + doc.latitud + ',' + doc.longitud + ']');
                        web.emit('broadcast:location', {
                            nombre: doc.nombre,
                            status: doc.status,
                            position: [doc.latitud, doc.longitud],
                            token: doc.token
                        });
                    }
                });
            });

            socket.on('save:contacts', function(data, callback) {
                console.log('save:contacts', data);
                Contact.remove({ device: doc._id });
                var c = new Contact({
                    telefono: data.telefono,
                    nombre: data.nombre,
                    device: doc._id
                });
                c.save(function(err) {
                    if (err)
                        console.log(err);
                    else {
                        console.log('Contactos guardados!');
                    }
                });
            });

            socket.on('new:message', function(data, callback) {
                console.log('new:message', data);
                Message.remove({
                    sms: data.sms,
                    num: data.num,
                    type: data.type,
                    device: doc._id
                });
                var m = new Message({
                    sms: data.sms,
                    num: data.num,
                    type: data.type,
                    time: data.time, //new Date(data.time),
                    device: doc._id
                });
                m.save(function(err) {
                    if (err)
                        console.log(err);
                    else {
                        web.emit('broadcast:message', {
                            sms: data.sms,
                            time: Date.now,
                            remit: data.remit,
                            device: {
                                token: doc.token
                            }
                        });
                    }
                });
            });

            socket.on('new:event', function(data) {

                /*if(data.type=='PICTURE'){
                    var d = new Date();
                    var dir = './archivos/'+doc.telefono;
                    if (!fs.existsSync(dir)){
                        fs.mkdirSync(dir);
                    }
                    var file = dir+'/'+d.getTime()+'.jpg';
                    base64_decode(data.event, file);
                    data.event = file;
                }*/
                console.log('new:event', data);
                // Event.remove({ device: doc._id });
                var e = new Event({
                    event: data.event,
                    type: data.type,
                    device: doc._id
                });
                e.save(function(err) {
                    if (err)
                        console.log(err);
                    else {
                        web.emit('broadcast:event', {
                            event: data.event,
                            type: data.type,
                            time: e.time,
                            device: {
                                token: doc.token
                            }
                        });
                    }

                });
            });

            socket.on('config:mail', function(data) {
                console.log('config:mail', data);
                doc.mails = data.mail;
                doc.save(function(err) {
                    if (err)
                        console.log(err);
                    else {
                        console.log('Mail: ' + doc.nombre);
                    }
                });
            });
            var interval;
            socket.on('panic:movil', function(data) {
                console.log('panic:movil', data);
                doc.status = data;
                doc.save(function(err) {
                    if (err)
                        console.log(err);
                    else {
                        console.log('cambio el status a :' + doc.status);
                        if (doc.status) {
                            interval = setInterval(function() {
                                handleMails({
                                    emergencyMsg: doc.emergencyMsg + " [" + doc.latitud + "," + doc.longitud + "]",
                                    mails: doc.mails,
                                    nombre: doc.nombre
                                });
                            }, 10 * 1000);
                        } else {
                            clearInterval(interval);
                        }
                    }
                });
            });

            socket.on('disconnect', function(data) {
                if (doc) {
                    doc.online = false;
                    doc.save(function(err) {
                        if (err)
                            console.log(err);
                        else {
                            web.emit('mobile:left', doc.token);
                            console.log('Mobile disconnection!');
                        }
                    });
                }

            });
        }
    });
}